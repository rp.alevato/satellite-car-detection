import numpy as np
import cv2
import gc
import csv

# Class needed for reading the pickle object
class CarProp:
    def __init__(self,phase,type,loc_1,loc_2):
        self.phase  = phase
        self.type   = type
        self.loc_1  = loc_1
        self.loc_2  = loc_2

# Save image crops and csv annotations
def create_crops_labels(city_name, city_img_name, cars, row_size, col_size):
    print("Loading Image "+city_name+"/"+city_img_name)
    raw_img = (cv2.imread("raw_dataset/"+city_name+"/"+city_img_name+".png"))
    print("Image Shape (row, col, color):", raw_img.shape)
    # Mark labels on blank img map
    raw_img_labels = np.empty((raw_img.shape[0], raw_img.shape[1]))
    for car in cars:
        raw_img_labels[int(car.loc_2), int(car.loc_1)] = 1
    print("Done")
    print("Creating crops and CSV annotations")
    crop_row_size = row_size
    crop_col_size = col_size
    mod_row_size = raw_img.shape[0] % crop_row_size
    mod_col_size = raw_img.shape[1] % crop_col_size
    if (mod_row_size != 0) or (mod_col_size != 0):
        raw_img = cv2.copyMakeBorder(
            raw_img, top=0, bottom=(crop_row_size - mod_row_size),
            left=0, right=(crop_col_size - mod_col_size),
            borderType=cv2.BORDER_CONSTANT, value=[0,0,0])
    row_max = raw_img.shape[0]
    col_max = raw_img.shape[1]
    csv_labels = []
    for i, col_init in enumerate(range(0, col_max, crop_col_size)):
        col_end = col_init + crop_col_size
        for j, row_init in enumerate(range(0, row_max, crop_row_size)):
            row_end = row_init + crop_row_size
            crop_name = city_name+"__"+city_img_name+"__"+format(i,'02d')+"x"+format(j,'02d')+".jpg"
            crop = raw_img[row_init:row_end, col_init:col_end].copy()
            cv2.imwrite("crop_dataset/"+crop_name, crop)
            crop_labels = np.nonzero(raw_img_labels[row_init:row_end, col_init:col_end])
            for k in range(len(crop_labels[0])):
                row1 = crop_labels[1][k] - 18 if (crop_labels[1][k] - 18) > 0 else 0
                row2 = crop_labels[1][k] + 18 if (crop_labels[1][k] + 18) < crop_col_size else crop_col_size
                col1 = crop_labels[0][k] - 18 if (crop_labels[0][k] - 18) > 0 else 0
                col2 = crop_labels[0][k] + 18 if (crop_labels[0][k] + 18) < crop_row_size else crop_row_size
                csv_labels.append([crop_name, row1, col1, row2, col2, "car"])
                cv2.rectangle(crop, (row1, col1), (row2, col2), (0,0,255), 3)
            cv2.imwrite("annotated_dataset/"+crop_name, crop)
    with open("annotations.csv", 'a', newline='') as csv_file:
        writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
        for csv_label in csv_labels:
            writer.writerow(csv_label)
    print("Done\n")

    del crop, csv_labels, raw_img, raw_img_labels
    gc.collect()
    return
