import pickle
import numpy as np
import csv
import os
import shutil
import cv2
from create_crops_labels import CarProp, create_crops_labels

# Load objects from pickle
objects = "Objects_All.pickle"
print("Loading " + objects + "...")
infile = open(objects,'rb')
cars = pickle.load(infile)
infile.close()
print("Done\n")

# Create needed CSV files and clean crop folders
with open("classes.csv", 'w', newline='') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
    writer.writerow(["car", 0])
with open("annotations.csv", 'w', newline='') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
if not os.path.exists("crop_dataset"):
    os.mkdir("crop_dataset")
else:
    shutil.rmtree('crop_dataset')
    os.mkdir("crop_dataset")
if not os.path.exists("annotated_dataset"):
    os.mkdir("annotated_dataset")
else:
    shutil.rmtree('annotated_dataset')
    os.mkdir("annotated_dataset")

# List with Cities Images to use
cities = ["Utah_AGRC", "Toronto_ISPRS", "Potsdam_ISPRS"]

# Create the crops
for city_name in cities:
    city_img_names = cars[city_name].keys()
    for city_img_name in city_img_names:
        create_crops_labels(city_name, city_img_name, cars[city_name][city_img_name], 790, 790)

# Randomizing Train, Val and Test Sets
# Read CSV for randomizing sets
annotations = []
with open("annotations.csv", 'r', newline='') as csv_file:
    reader = csv.reader(csv_file)
    for row in reader:
        annotations.append(row)
        annotations[-1][0] = "dataset/"+annotations[-1][0]
        annotations[-1][1] = int(annotations[-1][1])
        annotations[-1][2] = int(annotations[-1][2])
        annotations[-1][3] = int(annotations[-1][3])
        annotations[-1][4] = int(annotations[-1][4])
# Sets Proportions
train_prop = 0.5
val_prop = 0.3
# Read only image names
img_names = [row[0] for row in annotations]
img_names = list(set(img_names))
# Sort and seed to get always same sets
img_names.sort()
annotations.sort()
np.random.seed(10)
np.random.shuffle(img_names)
# Select annotations based on images
n_imgs = len(img_names)
n_train = int(n_imgs * train_prop)
n_val = int(n_imgs * val_prop)
train_imgs = img_names[0:n_train]
val_imgs = img_names[n_train:(n_train+n_val)]
test_imgs = img_names[(n_train+n_val):]
# Save new annotations on CSV
with open("train_annotations.csv", 'w', newline='') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
    for annotation in annotations:
        if annotation[0] in train_imgs:
            writer.writerow(annotation)
with open("val_annotations.csv", 'w', newline='') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
    for annotation in annotations:
        if annotation[0] in val_imgs:
            writer.writerow(annotation)
with open("test_annotations.csv", 'w', newline='') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
    for annotation in annotations:
        if annotation[0] in test_imgs:
            writer.writerow(annotation)
