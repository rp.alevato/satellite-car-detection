# Project for Satellite Car Detection in COWC Dataset

Download dataset cowc-m and get raw images and pickle annotations on:
> https://gdo152.llnl.gov/cowc/download/cowc-m-everything.txz

This implementation only utilizes Toronto, Utah and Potsdam raw files. Save on the respectives folders on raw_dataset.

Run create_multiple_crops_labels.py to create crops and annotated datasets. The CSV annotations are in the format for fizyr retinanet's implementation on:
> https://github.com/fizyr/keras-retinanet

After this zip the crop_dataset content (just the images) and upload it to Google Drive. On the same folder upload any jupyter notebook and the csv files. Open the jupyter notebook on Google Colab and follow the instructions.

PS: The mod jupyter notebook applies a histogram equalization on the dataset. We found that this is useless as the F1 Score is approximately the same, so this transformation and time lost is not needed. Also we found that training for 10 Epochs is ideal given the other settings in the notebook.
